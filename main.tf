provider "aws" {
  region = "eu-central-1"
}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}
variable instance_type {}
variable public_key_location {}
variable private_key_location {}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

# create new route table
resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
  tags = {
    Name: "${var.env_prefix}-rtb" 
  }
}

# create new internet gateway
resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id
  tags = {
    Name: "${var.env_prefix}-igw" 
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
} 

resource "aws_security_group" "myapp-sg" {
  name = "myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id

  # incoming
  ingress = [  
    {
      description = ""
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = ""
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [var.my_ip]
    }
  ]

  # outcoming
  egress {  
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  
  tags = {
    Name: "${var.env_prefix}-sg" 
  }
}

# Getting linux image from Amazon
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

// get ad see value in the shell
output "aws_ami_id" {
  # value = data.aws_ami.latest-amazon-linux-image
  value = data.aws_ami.latest-amazon-linux-image.id
}

output "ec2_public_ip" {
  # value = data.aws_ami.latest-amazon-linux-image
  value = aws_instance.myapp-server.public_ip
}

resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  # user_data is much better than provisioners, terraform does not recommend to use provisioner
  # user_data = file("entry-script.sh")

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = file(var.private_key_location)
  }

  # copy file from local machine (terraform instance) to a new created server
  provisioner "file" {
    source = "entry-script.sh"
    destination = "/home/ec2-user/entry-script-on-ex2.sh"
  }

  # execute on a new created server
  provisioner "remote-exec" {
    script = user_data = file("entry-script.sh")
    # inline = [
    #   "export ENV=dev",
    #   "mkdir newdir"
    # ]
  }

  # invokes a local executable after a resource is created
  provisioner "local-exec" {
    command = "echo ${self.public_ip} > output.txt"
  }

  tags = {
    Name: "${var.env_prefix}-server"
  }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Confirm without approve command
# terraform apply -auto-approve

# Destroy complete infrastructure
# terraform destroy

# "3.120.244.202"
# ssh -i ~/.ssh/dev-server-key.pem ec2-user@3.120.244.202

# terraform state show aws_instance.myapp-server

# ssh -i ~/.ssh/id_ed25519 ec2-user@3.120.244.202
# ssh ec2-user@18.195.155.150
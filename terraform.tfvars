vpc_cidr_block = "10.0.0.0/16"
subnet_cidr_block = "10.0.10.0/24"
avail_zone = "eu-central-1b"
env_prefix = "dev"
my_ip = "95.26.77.239/32"
instance_type = "t2.micro"
public_key_location = "/Users/emilmakaev/.ssh/id_ed25519.pub"
private_key_location = "/Users/emilmakaev/.ssh/id_ed25519"
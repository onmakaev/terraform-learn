# Two ways to set credentials values
# 1) export AWS_SECRET_ACCESS_KEY=AWS_SECRET_ACCESS_KEY
# export AWS_ACCESS_KEY_ID=AWS_ACCESS_KEY_ID
# terraform apply -var-file terraform-dev.tfvars
# terrafor automatikly find and use ENV values AWS_SECRET_ACCESS_KEY and AWS_ACCESS_KEY_ID
# this way of setting credentials values will lost if we change terminal window

# 2) second way, set credentials in aws file ~/.aws/credentials
# aws configure
# also we can set region etc. values
provider "aws" {
  # region = "eu-central-1"
  # access_key = "access_key"
  # secret_key = "secret_key"
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Use variables
# Three ways to set variable
# 1) command line after execute command "terraform apply" Enter value: 10.0.20.0/24
# 2) the command line when execute command, we prefine variable
# terraform apply -var "subnet_cidr_block=10.0.30.0/24"
# 3) Best way of setting variables. Create file "terraform.tfstate" and execute command "terraform apply"

# for enviroment execution, change file name to "terraform-dev.tfvars"
# command will be "terraform apply -var-file terraform-dev.tfvars"

# default value will be use if terraform will not find value
variable "subnet_cidr_block" {
  # type = string
  # type = bool
  # type = number
  # type = list(string)
  default = "10.0.10.0/24"
  description = "subnet cidr block"
}

# example with list of variables
variable "cidr_blocks" {
  description = "cidr blocks for vpc and subnets"
  # type = list(string)
  type = list(object({
    cidr_block = string,
    name = string
  }))
}

variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
}

variable "environment" {
  description = "deployment environment"
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
resource "aws_vpc" "development-vpc" {
  # cidr_block = var.vpc_cidr_block
  # cidr_block = var.cidr_blocks[0]
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name = var.cidr_blocks[0].name
  }
}

# SET TERRAFORM ENVIRENMET VARIABLE
# export TF_VAR_avail_zone="eu-central-1a"
variable avail_zone {}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  # cidr_block = var.subnet_cidr_block
  # cidr_block = var.cidr_blocks[1]
  cidr_block = var.cidr_blocks[1].cidr_block
  # availability_zone = "eu-central-1a"
  availability_zone = var.avail_zone
  tags = {
    Name = var.cidr_blocks[1].name
  }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Confirm without approve command
# terraform apply -auto-approve

# Destroy complete infrastructure
# terraform destroy

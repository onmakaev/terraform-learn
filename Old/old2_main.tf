provider "aws" {
  region = "eu-central-1"
  access_key = "access_key"
  secret_key = "secret_key"
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
resource "aws_vpc" "development-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "development",
    # vpc_env = "dev"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = "10.0.10.0/24"
  availability_zone = "eu-central-1a"
  tags = {
    Name = "subnet-1-dev"
  }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Define what value we want terraform to output at the end
# of applying out configuration from one if the resources
# Example we want to output the Ids of every resource that we are creating.
# after apply command, in the end of console, we will see output information
output "dev-vpc_id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}

# Initializes a working directory
# Installs providers defined in the Terraform configuration
# terraform init

# Apply our resources
# terraform apply

# Confirm without approve command
# terraform apply -auto-approve

# Destroy part of infrastructure
# terraform destroy -target aws_subnet.dev-subnet-2

# Destroy complete infrastructure
# terraform destroy

# Check difference configure static and current state
# terraform plan

# Looking terraform state
# terraform state
# terraform state list
# terraform state show aws_subnet.dev-subnet-1

# Check which attributes are available for each resoure
# terraform plan

// 20:25
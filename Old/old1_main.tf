# provider = import library
# https://registry.terraform.io/providers/hashicorp/aws/latest
provider "aws" {
  region = "eu-central-1"
  access_key = "access_key"
  secret_key = "secret_key"
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# resource/data = function call of library
# arguments = parameters of function
# creating VPC in AWS
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
resource "aws_vpc" "development-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "development",
    # vpc_env = "dev"
  }
}

# creating subtet in AWS for VPC (development-vpc)
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = "10.0.10.0/24"
  availability_zone = "eu-central-1a"
  tags = {
    Name = "subnet-1-dev"
  }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Example, we want to create subner for already existing VPC, so
# we should create DATA and get from there ID of VPC
# if set (default = true), so we get default VPC, with IP 172.31.0.0/16
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc
data "aws_vpc" "existing_vpc" {
  default = true
}

# if we want to remove something in this file and from AWS, we have 2 options
# first option (best way) - just remove core from this file and execute - terraform apply
# second option from bash-shell command - terraform destroy -target aws_subnet.dev-subnet-2
resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing_vpc.id
  cidr_block = "172.31.48.0/20"
  availability_zone = "eu-central-1a"
  tags = {
    Name = "subnet-2-default"
  }
}

# Initializes a working directory
# Installs providers defined in the Terraform configuration
# terraform init

# Apply our resources
# terraform apply

# Confirm without approve command
# terraform apply -auto-approve

# Destroy part of infrastructure
# terraform destroy -target aws_subnet.dev-subnet-2

# Destroy complete infrastructure
# terraform destroy

# Check difference configure static and current state
# terraform plan

# Looking terraform state
# terraform state
# terraform state list
# terraform state show aws_subnet.dev-subnet-1
